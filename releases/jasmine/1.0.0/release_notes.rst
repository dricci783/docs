.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../../../definitions.rst

Jasmine - 1.0.0
###############

About
*****

The objective of this document is to provide basic introductory information
about included functionalities, known issues, instructions guidance for
the Jasmine release of the |main_project_name| project.

The |main_project_name| project serves as a solid base foundation
for products. It is not a standalone product itself but rather a starting
project for other projects and products.

Within this release, we moved from the Incubation phase into the Developing phase. 
In practice, it means that |main_project_name| has become a member project
of the Eclipse Foundation. We believe that it enables us for further rapid 
development and extenstion in various markets.

Scope
*****

**Release codename**: Jasmine

**Release version**: 1.0.0

**Release timeframe**: 2021-04-13 .. 2021-12-20

The Objectives of the Release
-----------------------------

The objective of the *Jasmine* release is to prepare a solid ground for the Eclipse 
Foundation onboarding. That includes, but is not limited to:

- Project maturity gain
- Hardware & Software roadmap expansion
- IP compliance & policies deployment
- An extension of the available set of blueprints 

The List of Software Features Included
--------------------------------------

- Raspberry Pi Hardware Enablement
  (`REQ#253 <https://git.ostc-eu.org/OSTC/requirements/-/issues/253>`__)
- Support and default to the 5.10.x LTS Linux Kernel 
  (`REQ#99 <https://git.ostc-eu.org/OSTC/requirements/-/issues/99>`__)
- BSP: Add support for Nordic Semiconductor nRF52840 DK 
  (`REQ#248 <https://git.ostc-eu.org/OSTC/requirements/-/issues/248>`__)
- Net: Overall IPv6 connectivity for the gateway 
  (`REQ#96 <https://git.ostc-eu.org/OSTC/requirements/-/issues/96>`__)
- Net: Support open-source 802.11 stack on Linux OS 
  (`REQ#296 <https://git.ostc-eu.org/OSTC/requirements/-/issues/296>`__)
- IP Compliance: Openchain 2.1 compliant IP policy 
  (`REQ#240 <https://git.ostc-eu.org/OSTC/requirements/-/issues/240>`__)
- Experimental UI framework LVGL 
  (`REQ#279 <https://git.ostc-eu.org/OSTC/requirements/-/issues/279>`__)

Please note that there is a set of blueprints available which could be built using this 
release. However, we can not guarantee that all newly developed blueprints will work 
with the previous releases of the |main_project_name|.

In case of any doubts, please refer to a blueprint's documention or contact us using any 
of the communication channels available.

Supported Hardware Platforms
----------------------------

+------------------------------------------------------+-------------------+---------------------------------------+
| Board (chipset)                                      | Supported kernels | Board documentation                   |
+======================================================+===================+=======================================+
| Avenger96 (STM32 - Cortex-A7 & Cortex-M4)            | Linux & Zephyr    |                                       |
+------------------------------------------------------+-------------------+---------------------------------------+
| Arduino Nano (nRF52840 - Cortex-M4)                  | Zephyr            | :ref:`SupportedBoardArduinoNano33BLE` |
+------------------------------------------------------+-------------------+---------------------------------------+
| Nitrogen (nRF52832 - Cortex-M4)                      | Zephyr            | :ref:`SupportedBoardNitrogen`         |
+------------------------------------------------------+-------------------+---------------------------------------+
| nRF52840-DK (nRF52840 - Cortex-M4)                   | Zephyr            | :ref:`SupportedBoardnRF52840DK`       |
+------------------------------------------------------+-------------------+---------------------------------------+
| RaspberryPI 4b (BCM2711)                             | Linux             | :ref:`raspberrypi`                    |
+------------------------------------------------------+-------------------+---------------------------------------+
| Seco SBC-B68-eNUC (Intel x86)                        | Linux             | :ref:`SupportedBoardSecoB68`          |
+------------------------------------------------------+-------------------+---------------------------------------+
| Seco SBC-C61 (NXP i.MX 8M - Coretex-A53 & Cortex M4) | Linux             | :ref:`SupportedBoardSecoC61`          |
+------------------------------------------------------+-------------------+---------------------------------------+

Test Report
-----------

Not available for this release.

Installation
************

:ref:`Quick Build <OniroQuickBuild>` provides an example of how to build
the |main_project_name| project for a supported target. Visit the
:ref:`Hardware Support <HardwareSupport>` section for instructions on how to
build for other supported targets.

Visit :ref:`setting up a repo workspace <RepoWorkspace>` for instructions how to prepare 
workspace for development. 
Since |main_project_name| uses repo tool for its development, you can use the release tag
for the repo init commands as follows:

.. code-block:: console

    repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b v1.0.0

Known Issues
------------

1. The Avenger96 target (stm32mp1-av96) does not ship with firmware files for Bluetooth, 
   WiFi, and the image processor by default. We are working on a solution to 
   re-distribute them during the normal image build for this board.
2. Adding package_management feature breaks passwordless root login.
3. Smart Panel blueprint - Avenger96 HDMI Display problems.

Source Code Available
---------------------

For more details on our repo structure, see:

`OniroProject's GitLab <https://gitlab.eclipse.org/eclipse/oniro-core>`__ document

The release is signed with the key:

.. code-block::

   -----BEGIN PGP PUBLIC KEY BLOCK-----
   mDMEYaTV2hYJKwYBBAHaRw8BAQdAhCeM7i/qPIdJMs9SjBAYYsZ0lEhkJExfScVV
   3PCAMNy0RU9uaXJvIFByb2plY3QgSmFzbWluZSBSZWxlYXNlIFNpZ25pbmcgS2V5
   IDxzdGVmYW4uc2NobWlkdEBodWF3ZWkuY29tPoiWBBMWCAA+FiEEg6LprPlSFnVN
   ljMIypzNhbi9XNYFAmGk1doCGwMFCQYGewAFCwkIBwIGFQoJCAsCBBYCAwECHgEC
   F4AACgkQypzNhbi9XNa2UwD9El6WIkKBoZ/xAOdg/eUTe4ltzjyrMznGTjltuLS8
   iq0A/iBrx+e9Jod5j3t9U5a99BTUWEe1hHi2lyAeGyCgvyoHuDgEYaTV2hIKKwYB
   BAGXVQEFAQEHQJ+oeI2KvnvYtIixZc6uZGA6fhNFrBIQdVk65jU6/GcFAwEIB4h+
   BBgWCAAmFiEEg6LprPlSFnVNljMIypzNhbi9XNYFAmGk1doCGwwFCQYGewAACgkQ
   ypzNhbi9XNYaagD/eslWmRnEyp12tlLYKamSFCXHbescDpFIEG4SZ3tSG/gBAJj+
   UYCroIIYHatvtVRtCmLXFRvx7eXmrFtzBJV7oXAH
   =XpqZ
   -----END PGP PUBLIC KEY BLOCK-----

DevOps Infrastructure
*********************

To learn more about our approach to CI (Continuous Integration) strategy used for this 
release, please see:

:doc:`/ci/index` document

Testing
-------

For more details please refer to:

:doc:`/ci/device-testing` document

Contributions
*************

If you are a developer eager to know more details about |main_project_name| or
just an enthusiast with a patch proposal, you are welcome to participate in our 
|main_project_name| ecosystem development. To do so, please sign-up using the process 
described below:

:doc:`/contributing/index` document

License
*******

Project manifest as well as project-specific meta-layers, recipes and
software packages are, unless specified otherwise, published under
Apache 2.0 license. The whole operating system built by users from the
project manifest is an aggregate comprised of many third-party components
or component groups, each subject to its own license conditions.

Official project release includes only the project manifest as well as
project-specific meta-layers, recipes.
Any reference binary image, build cache, and any other build
artifacts are distributed as a convenience only and are not part of the
release itself.
